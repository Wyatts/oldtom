import std.file;
import std.stdio;
import oldtom;
import jsvar;
import toml :TOML;

void main(){
	//auto finput = File("unit.toml","r");
	string input = `
	name = "Vagabond"
	[personal.traits]
		face = "neutral"
		scar = false
	[personal.traits.limbs]
		arms = 2
		legs = 2
	[personal.numbers]
		lucky = [ 7, 13 ]
	`;

	writeln(TOML(cast(string) read("./samples/unit.toml")));

	var result = toTree(input);
	writeln("Text reconstruction from string");
	writeln("-------------------------------");
	auto finput = cast(string)(read("./samples/unit.toml"));
	writeln(toTOML(finput));
	writeln("Some selected values");
	writeln("--------------------");
	writeln("result:\n\t",result);
	writeln("result.personal:                     ",result.personal);
	writeln("result.personal.traits:              ",result.personal.traits); // BUG: Misses personal.traits
	writeln("result.personal.numbers.lucky[1]:       ",result.personal.numbers.lucky[1]);
	writeln("result.personal.get!(string[string]).keys:	",result.personal.get!(string[string]).keys);

}
