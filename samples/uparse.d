import std.file;
import std.range;
import std.stdio;
import oldtom;
import jsvar;
import toml :TOML;

void main(){
	auto finput = cast(string)(read("vagabond.toml"));
	writeln(TOML(finput));

	var result = toTree(finput);

	writeln("Text reconstruction:");
	writeln("-------------------------");
	writeln(toTOML(finput));
	writeln("-------------------------");
	writeln("result:\n\t",result);
	writeln("result.unit.name:\n\t",result.unit.name);
	writeln("result.unit.printsym:\n\t",result.unit.printsym);
	writeln("result.stats:\n\t",result.stats);
	import std.conv : to;
	switch(to!string(result.unit.faction)){											 
	case "ALLY":
		writeln("ALLY");
		break;
	case "ENEMY":
		writeln("ENEMY");
		break;
	default:
		"Default faction fallthrough".writeln;
		break;
	}
	foreach(k ; result.stats.get!(string[string]).keys)
	    writefln("Key: %s = %s",k,result.stats.get!(string[string])[k]);
	//foreach(t;result.stats)
	    //writeln(t);






}
