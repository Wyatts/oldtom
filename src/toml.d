

import pegged.peg;
import std.algorithm: startsWith;
import std.functional: toDelegate;
public import pegged.grammar : grammar;

//TODO: Try to find a way to discard Element and Group; they're superfluous
//	I mean, just look at the parse tree!
enum g = grammar("
TOML:
Element	< (Group / Key)* :eoi
Group <- :OPEN Metagroup # This only needs to stay if I want to keep text output.
Metagroup <- identifier ( (:'.' Metagroup) / CLOSE (!Group Key)* ) #Might be better served with '< '
Key	<  ^identifier :'=' (String / Dtime / Float / Integer / Bool / Array)

#Types
String	<~ :doublequote ~(!doublequote Char)* :doublequote
Integer	<~ '-'? digits
Float	<~ '-'? digits '.' digits
Bool	<- 'true' / 'false'
Dtime	<~ digit digit digit digit
	           '-' digit digit
	           '-' digit digit
	           'T' digit digit
	           ':' digit digit
	           ':' digit digit 'Z'

Array	< :OPEN %AType (:',' %AType)* :CLOSE #Shouldn't allow mixed types
AType	< StringA / IntA / FloatA / BoolA / DtimeA / %Array
StringA	< String (:',' String)*
IntA	< Integer (:',' Integer)*
FloatA	< Float (:',' Float)*
BoolA	< Bool (:',' Bool)*
DtimeA	< Dtime (:',' Dtime)*

#Terminals
Char	<~ Special{lowerSpecial} / .
Special	<~ backslash ( 'b' / 't' / 'n' / 'f' / 'r' 
	             / doublequote / slash / backslash 
	             / 'u' hexDigit hexDigit hexDigit hexDigit
	             / 'U' hexDigit hexDigit hexDigit hexDigit hexDigit hexDigit hexDigit hexDigit)
OPEN	< '['
CLOSE	< ']'
Comment	<- '#' (!eol .)* :eol
Spacing	<: ( blank / Comment )* #Why doesn't ( spacing / Comment )* work here?
");

PT lowerSpecial(PT)(PT p) {
	if (p.successful) {
		final switch (p.matches[0][0..2]){ //slice because of unicode
			case "\\b":
				p.matches[0] = "\b";
				break;
			case "\\t":
				p.matches[0] = "\t";
				break;
			case "\\n":
				p.matches[0] = "\n";
				break;
			case "\\f":
				p.matches[0] = "\f";
				break;
			case "\\r":
				p.matches[0] = "\r";
				break;
			case "\\\"":
				p.matches[0] = "\"";
				break;
			case "\\/":
				p.matches[0] = "/";
				break;
			case "\\\\":
				p.matches[0] = "\\";
				break;
			case "\\u":
				goto case;
			case "\\U":
				//https://github.com/dlang/dmd/blob/master/src/lexer.d
				import std.utf: encode;
				import std.conv: to;
				import std.exception:  assumeUnique;

				char[4] buf;
			        dchar codepoint = to!uint(p.matches[0][2..$],16);
        			auto seqSize = encode(buf,codepoint);
			        p.matches[0] = buf[0..seqSize].assumeUnique;
				break;
		}
	}
	return p;
}
mixin(g);

//#	AType	< HomArray(String) / StringA / IntA / FloatA / BoolA / DtimeA / Array
//#	AType	< HArray(String) / HArray(IntA) / HArray(FloatA) / HArray(BoolA) / HArray(DtimeA) / HArray(Array)
//#	HArray(Type) < Type (:',' Type)*
