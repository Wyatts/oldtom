module oldtom;
import std.conv;
import std.stdio;

import jsvar;
import pegged.peg : ParseTree;

import toml;

private bool open = false; //Are we in a group definition?

var toTree(string p){
	return toHash(TOML(p));
}

var toHash(ParseTree p)//Recursive; don't feel like changing the name right now.
{
	var ret = var.emptyObject;
	final switch(p.name){
		case "TOML":
			ret = toHash(p.children[0]);
			break;
		case "TOML.Element":
			foreach(k, child; p.children){
				var tmp = var.emptyObject; // breaks if not init
				string match = child.matches[0]; // match is a potential conflict in ret
				tmp[match] = toHash(child);

				if(match in ret.get!(string[string]))
					mergeTree(tmp, ret, match);
				else
					ret[match] = tmp[match];
			}
			break;
		case "TOML.Group":
			ret = toHash(p.children[0]);
			break;
		case "TOML.Metagroup":
			foreach(k, child; p.children[]){
				if(child.name == "TOML.CLOSE"){
					continue;
				}
				ret[child.matches[0]] = toHash(child);
			}
			break;
		case "TOML.Key":
			ret = toHash(p.children[1]); //The 0th is the identifier node
			break;
		case "TOML.String":
			ret = to!string(p.matches[0]);
			break;
		case "TOML.Integer":
			ret = to!int(p.matches[0]);
			break;
		case "TOML.Float":
			ret = to!float(p.matches[0]);
			break;
		case "TOML.Bool":
			ret = to!bool(p.matches[0]);
			break;
		case "TOML.Dtime":
			ret = to!string(p.matches[0]);
			break;
		case "TOML.Array":
			if(p.children.length >= 2){
				ret = var.emptyArray;
				foreach(idx, child; p.children)
				    ret[idx] = toHash(child);
			}
			else ret = toHash(p.children[0]);
			break;
		case "TOML.StringA":
		case "TOML.IntA":
		case "TOML.FloatA":
		case "TOML.BoolA":
		case "TOML.DtimeA":
			ret = var.emptyArray;
			foreach(idx, child; p.children){
				ret[idx] = toHash(child);
			}
			break;
	}
	return ret;
} //end toHash */

string toTOML(string p){
	return toTOML(TOML(p));
}
string toTOML(ParseTree p){
	//Possible improvement: Indenting. Define global iLevel k and prepend k indentations to concat.
	switch(p.name){
		case "TOML":
			return toTOML(p.children[0]);
		case "TOML.Element":
			string result;
			foreach(child; p.children){
				result ~= toTOML(child);
			}
			return result;
		case "TOML.Group":
			string result = "\n[";
			open = true;
			result ~= toTOML(p.children[0]);
			foreach(child; p.children[1..$]){
				result ~= "\t"~toTOML(child);
			}
			return result;
		case "TOML.Metagroup":
			string result = p.matches[0];
			foreach(child; p.children){
				if(open)
					result ~= ".";
				
				result ~= toTOML(child);
			}
			return result;
		case "TOML.CLOSE":
			string result = "]\n"; // Yes, the character at the beginning of this
			open = false;            // literal is a backspace. I'm a bad person.
			foreach(child; p.children){
				result ~= "\t"~toTOML(child);
			}
			return result;
		case "TOML.Key":
			return toTOML(p.children[0])~" = "
			       ~ toTOML(p.children[1])
			       ~"\n";
		case "TOML.String":
			return "\""~p.matches[0]~"\"";
		case "TOML.Integer":
			return p.matches[0];
		case "identifier":
			return p.matches[0];
		case "TOML.Float":
			return p.matches[0];
		case "TOML.Bool":
			return p.matches[0];
		case "TOML.Dtime":
			return p.matches[0];
		case "TOML.Array":
			string result;
			if(p.children.length > 1) result ~= "[ ";
			foreach(child; p.children){
				result ~= toTOML(child)~", ";
			}
			result.length -=2; //To cut off the extra ones added
			if(p.children.length > 1) result ~= " ]";
			return result;
		case "TOML.AType":
			string result;
			foreach(child; p.children){
				result ~= toTOML(child);
			}
			return result;
		case "TOML.StringA":
		case "TOML.IntA":
		case "TOML.FloatA":
		case "TOML.BoolA":
		case "TOML.DtimeA":
			string result = "[";
			if(p.children.length == 1)
				result ~= " "~ toTOML(p.children[0]);
			else{
				auto alen = p.children.length;
				foreach(i, child; p.children){
					result ~= " "~toTOML(child);
					if(i+1 < alen) result ~= ",";
				}
			}
			return result ~= " ]";
		default:
			return "\n";
	}
} //end toTOML */

void mergeTree(ref var tmp, ref var ret, string match){
	foreach(str; tmp){ //tmp should only ever be one element, but this makes isolating it easier
		//Okay, this is ass-ugly, but I'm sick of this.
		// First, get the branch name we're trying to append.
		string nodeName = str.get!(string[string]).keys[0];
		if(nodeName in ret[match].get!(string[string])){
			// We have another conflict; recurse
			mergeTree(tmp[match], ret[match], nodeName);
		}
		else
			ret[match][nodeName] = str[nodeName];
		// var can be trated as a multidimensional array, thank god.
	}
}
