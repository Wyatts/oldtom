CC=dmd

#XXX: This is such an ugly hack, but Dub lacks most sorts of useful introspection, so it's necessary for now.
PEGGED_PATH=$(HOME)/.dub/packages/pegged-0.2.1
PEGGED_FILES=-I$(PEGGED_PATH) -L-L$(PEGGED_PATH) -L-lpegged

OLDTOM_PATH=$(shell echo $(PWD)| sed 's/\(\/oldtom\).*/\1/g')
ALL_FILES=-I$(OLDTOM_PATH)/src -L-L$(OLDTOM_PATH) -L-loldtom $(PEGGED_FILES)
#DFLAGS="-g -debug -odobj -unittest"

all: deps lib sample

deps:
	dub fetch pegged --version=0.2.1

sample: lib
	cd samples ; $(CC) extexample.d $(ALL_FILES) -of../jtomltest ; cd ..

unit: lib
	cd samples ; $(CC) uparse.d $(ALL_FILES) -of../youparse ; cd ..

lib: deps
	cd src ; $(CC) oldtom.d toml.d jsvar.d $(PEGGED_FILES) -lib -of../liboldtom.a ; cd ..

clean:
	rm -f *.o *.a jtomltest youparse
